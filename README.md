# ShutterSnitch Metadata Importer Plug-in for Lightroom Classic #

### Import metadata from CSV files or via network ###

You can export CSV files from ShutterSnitch via *selection mode > share button > Selection List > Share*.
Make sure *Options > Settings > Advanced tab > Selection List > Sharing Format* is set to CSV.

Network import and sync is supported in ShutterSnitch from v5.4 and is a subscription feature. ShutterSnitch must be open and your iOS device has to be connected to the same network as your computer.

### Imports versus Sync ###

* Imports gives you greater control over where and when you want to import metadata.
* Sync keeps track of changes in your ShutterSnitch collections. When you request a sync, it updates your catalog with any metadata changed in ShutterSnitch since the previous sync.

### Limiting which fields to update ###

By default only the *Rating*, *Description*, *Byline*, and *Capture Time* metadata fields are shared by ShutterSnitch.

The plug-in imports all the fields present in the CSV file, so simply add the fields you want updated and remove the ones you don't.

If you want to share all 60 supported fields, go to *Options > Settings > Advanced tab > Selection List* and tap the *Reset* button. Then select *Lightroom Classic Plug-in Items*.

***Filename* is obligatory for this plug-in to work.**

Keep the *Capture Time* for more precise file matching when applying the metadata. This is its sole purpose and it's not applied to files.

### Installing the plug-in ###

Install via *File > Plug-in Manager > Add* in Lightroom.

If you want to import or sync via your network:

* Make sure your iOS device is on the same network as your computer.
* Open ShutterSnitch and tap the title bar so the info bar drops down. Tap the right side of the info bar to bring up the *Wi-Fi Quick Connect* sheet. At the bottom of the networks list you'll see your *hostname*.
* Put in the hostname in the *ShutterSnitch IP address / Hostname* field in the Lightroom plug-in settings.

If for some reason you can't use or find the hostname, alternatively put in the IP address you see at the top of the screen of ShutterSnitch when entering a collection (Ex. 192.168.1.123). IP addresses change though, so a hostname is preferred.

Once installed the import and sync functions are available from the *Library > Plug-in Extras* menu.

### Troubleshooting ###

If you have issues wi-fi syncing, make sure your iOS device is connected to the same network as your computer, enter a collection in ShutterSnitch and then go to **http://***ShutterSnitch IP address / Hostname***:46786/lrplugin?** in a web browser on the computer running Lightroom. If you see a long string of characters, you're good. If not, your router could be blocking traffic on port 46786.