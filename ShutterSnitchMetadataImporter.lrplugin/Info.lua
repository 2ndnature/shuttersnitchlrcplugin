--[[----------------------------------------------------------------------------

ShutterSnitch Lightroom Classic Plug-in by Brian Gerfort
..who can't be held responsible for any data loss related to using this script.
This code is frolicking in the Public Domain.

------------------------------------------------------------------------------]]

return {

	VERSION = { major=1, minor=0, revision=2, },

	LrSdkVersion = 10.0,
	LrSdkMinimumVersion = 2.0,

	LrToolkitIdentifier = "com.2ndNature.ShutterSnitch-LR",
	LrPluginName = "ShutterSnitch",
	LrPluginInfoUrl="https://shuttersnitch.com/forums",

	LrLibraryMenuItems = {
		{
			title = "Import Metadata from CSV File",
			file = "ImportFromFile.lua",
		},
		{
			title = "Import Metadata from the Active Collection in ShutterSnitch",
			file = "ImportFromActiveCollection.lua",
		},
		{
			title = "Sync All Metadata Changes from ShutterSnitch",
			file = "SyncEverything.lua",
		},
	},
	
	LrPluginInfoProvider = 'PluginInfoProvider.lua',
}