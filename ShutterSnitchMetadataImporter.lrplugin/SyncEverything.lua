--[[----------------------------------------------------------------------------

ShutterSnitch Lightroom Classic Plug-in by Brian Gerfort
..who can't be held responsible for any data loss related to using this script.
This code is frolicking in the Public Domain.

------------------------------------------------------------------------------]]

require 'MetadataImporter'

local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'
local LrTasks = import 'LrTasks'
local LrPrefs = import 'LrPrefs'

LrTasks.startAsyncTask(function ()

	local prefs = LrPrefs.prefsForPlugin()
	local since = prefs.syncSince
	if since == nil then
		local f = LrView.osFactory()
		local popup = f:popup_menu {
			value = "1970",
			items = {
				{
					title = "1 second ago",
					value = "-1s",
				},
				{
					title = "1 week ago",
					value = "-1w",
				},
				{
					title = "1 month ago",
					value = "-1m",
				},
				{
					title = "1 year ago",
					value = "-1y",
				},
				{
					title = "The Beatles broke up",
					value = "1970",
				},
			},
		}
		local c = f:column {
			spacing = f:dialog_spacing(),
			f:row {
				f:static_text {
					title = "Your first sync can take a while depending on how far back\nin time you go and the amount of photos in your collections.",
					fill_horizontal = 1,
				},
			},
			f:row {
				f:static_text {
					title = "Sync metadata changes since:",
					alignment = 'left',
				},
				popup,
			},
		} 
		if LrDialogs.presentModalDialog { title = "Very First Sync", contents = c } == "ok" then
			if popup.value == "-1s" then
				since = os.time() - 1
			elseif popup.value == "-1w" then
				since = os.time() - (60 * 60 * 24 * 7)
			elseif popup.value == "-1m" then
				since = os.time() - (60 * 60 * 24 * 31)
			elseif popup.value == "-1y" then
				since = os.time() - (60 * 60 * 24 * 365)
			else
				since = 8588460
			end
		else
			return
		end
	end

	MetadataImporter.networkImport {
		query = string.format("since=%s", since),
		networkSyncRequestTime = os.time()
	}
		
end)
