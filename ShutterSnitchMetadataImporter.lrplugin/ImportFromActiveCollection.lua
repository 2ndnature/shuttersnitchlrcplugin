--[[----------------------------------------------------------------------------

ShutterSnitch Lightroom Classic Plug-in by Brian Gerfort
..who can't be held responsible for any data loss related to using this script.
This code is frolicking in the Public Domain.

------------------------------------------------------------------------------]]

require 'MetadataImporter'

local LrTasks = import 'LrTasks'

LrTasks.startAsyncTask(function ()

	if not MetadataImporter.preflightCheck() then
		return
	end

	MetadataImporter.networkImport {
		query = "coll=0",
	}

end)
