--[[----------------------------------------------------------------------------

ShutterSnitch Lightroom Classic Plug-in by Brian Gerfort
..who can't be held responsible for any data loss related to using this script.
This code is frolicking in the Public Domain.

------------------------------------------------------------------------------]]

local LrPrefs = import 'LrPrefs'
local LrDialogs = import 'LrDialogs'
local LrView = import "LrView"
local bind = LrView.bind
local share = LrView.share

local pluginInfoProvider = {}
local prefs = LrPrefs.prefsForPlugin()

local function settingsDidChange( propertyTable )

end

function pluginInfoProvider.sectionsForTopOfDialog(f, propertyTable )
	
	local latestSyncNeverTitle = "Latest sync: Never"
	if prefs.syncSince == nil then
		propertyTable.latestSyncTitle = latestSyncNeverTitle
	else
		propertyTable.latestSyncTitle = "Latest sync: " .. os.date('%x %H:%M', prefs.syncSince) -- '%Y-%m-%d %H:%M:%S'
	end
	return {
			{
				title = "General Settings",
				bind_to_object = propertyTable,
				f:row {
					f:checkbox {
						title = "Ask for confirmation before updating metadata",
						value = bind 'confirmBeforeUpdating',
					}
				},
				f:row {
					f:static_text {
						title = "This is Public Domain software. The author cannot be held responsible for any data loss.",
						fill_horizontal = 1,
					},
				},
			},
			{
				title = "Import Settings",
				bind_to_object = propertyTable,
				f:row {
	    			f:static_text {
    					title 			= "Limit updates to:",
    					alignment 		= 'left',
    				},
	    			f:popup_menu {
    					value 			= bind 'updateScope',
    					items			= {
    						{
    							title = "Active Collection or Folder",
    							value = "activeCollection"
    						},
    						{
    							title = "Entire Catalog",
    							value = "catalog"
    						},
    					},
	    			},
    			},
			},
			{
				title = "Sync Settings",
				bind_to_object = propertyTable,
				f:row {
					f:static_text {
						title = "A Sync updates matching photos in the entire catalog with modified metadata in any collection in ShutterSnitch.\nChoose which fields should be synced in ShutterSnitch: Options → Settings → Advanced tab → Selection List.",
						fill_horizontal = 1,
					},
				},
				f:row {
	    			f:static_text {
    					title 			= "ShutterSnitch IP address / Hostname:",
    					alignment 		= 'right',
    					width 			= share 'labelWidth',
    				},
	    			f:edit_field {
    					truncation 		= 'middle',
    					immediate 		= true,
    					fill_horizontal = 1,
    					value 			= bind 'hostAddress',
	    			},
    			},
    			f:row {
					f:static_text {
						title = bind 'latestSyncTitle',
						fill_horizontal = 1,
					},
        			f:push_button {
        				title 			= "Reset Sync State",
        				alignment 		= 'right',
        				action 			= function()
        					if LrDialogs.confirm("Reset Sync State", "This will force a complete resync of all metadata the next time you import via Wi-Fi.", "Reset", "Cancel") == "ok" then
	       						prefs.syncSince = nil
								propertyTable.latestSyncTitle = latestSyncNeverTitle
	       					end
        				end,
        			},   			
				},
			},
		}
end

function pluginInfoProvider.startDialog( propertyTable )

	propertyTable.confirmBeforeUpdating = prefs.confirmBeforeUpdating
	if propertyTable.confirmBeforeUpdating == nil then 
	   	propertyTable.confirmBeforeUpdating = true
	end

	propertyTable.updateScope = prefs.updateScope
	if propertyTable.updateScope == nil then 
	   	propertyTable.updateScope = "activeCollection"
	end

	propertyTable.hostAddress = prefs.hostAddress
	if propertyTable.hostAddress == nil then 
	   	propertyTable.hostAddress = ""
	end

	propertyTable:addObserver('confirmBeforeUpdating', settingsDidChange )
	propertyTable:addObserver('updateScope', settingsDidChange )
	propertyTable:addObserver('hostAddress', settingsDidChange )

end

function pluginInfoProvider.endDialog( propertyTable )

	prefs.confirmBeforeUpdating	= propertyTable.confirmBeforeUpdating
	prefs.updateScope	= propertyTable.updateScope
	prefs.hostAddress	= propertyTable.hostAddress

end

return pluginInfoProvider
