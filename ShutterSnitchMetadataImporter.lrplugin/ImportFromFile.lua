--[[----------------------------------------------------------------------------

ShutterSnitch Lightroom Classic Plug-in by Brian Gerfort
..who can't be held responsible for any data loss related to using this script.
This code is frolicking in the Public Domain.

------------------------------------------------------------------------------]]

require 'MetadataImporter'

local LrTasks = import 'LrTasks'
local LrDialogs = import 'LrDialogs'
local LrFileUtils = import 'LrFileUtils'

LrTasks.startAsyncTask(function ()

	if not MetadataImporter.preflightCheck() then
		return
	end

	local selectedFile = LrDialogs.runOpenPanel( { title = "Import Metadata", prompt = "Select", canChooseFiles = true, canChooseDirectories = false, canCreateDirectories = false, allowsMultipleSelection = false, fileTypes = "csv" } )
	if selectedFile ~= nil then
		MetadataImporter.importMetadataFromCSVLines {
			lines = LrFileUtils.readFile( selectedFile[1] ),
		}
	end

end)
