--[[----------------------------------------------------------------------------

ShutterSnitch Lightroom Classic Plug-in by Brian Gerfort
..who can't be held responsible for any data loss related to using this script.
This code is frolicking in the Public Domain.

------------------------------------------------------------------------------]]

MetadataImporter = {}

local LrFunctionContext = import 'LrFunctionContext'
local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'
local LrBinding = import 'LrBinding'
local LrApplication = import 'LrApplication'
local LrTasks = import 'LrTasks'
local LrHttp = import 'LrHttp'
local LrStringUtils = import 'LrStringUtils'
local LrPasswords = import 'LrPasswords'
local LrMD5 = import 'LrMD5'
local LrPrefs = import 'LrPrefs'

local prefs = LrPrefs.prefsForPlugin()
local progressScope = nil

local function explode(line, delimiter)
	local result = {}
	local begin = 1
	local delimiter_begin, delimiter_end = string.find(line, delimiter, begin)
	while delimiter_begin do
		table.insert(result, string.sub(line, begin, delimiter_begin - 1))
		begin = delimiter_end + 1
		delimiter_begin, delimiter_end = string.find(line, delimiter, begin)
	end
	table.insert(result, string.sub(line, begin))
	return result
end

function csvItems(line)
	local result = {}
	local item = ""
	local c, qbegin, qend, qnext
	local i = 1
	while i <= #line do
	    c = line:sub(i,i)
		if c == ';' then
			item = item:gsub("\\n", "\n")
			table.insert(result, item)
			item = ""
		else
			if item == "" and c == '"' then
				qnext = i
				repeat
					qbegin, qend = string.find(line, '";', qnext + 1)
					if qbegin then
						if select(2, string.gsub(line:sub(i + 1, qend - 2), '"', "")) % 2 == 0 then
							break
						end
						qnext = qend
					end
				until (qbegin == nil)
				if qbegin and qend then
					item = string.sub(line, i + 1, qbegin - 1)
					i = qend - 1
				else
					item = string.sub(line, i + 1, #line - 1)
					i = #line
				end
				item = item:gsub('""', '"')
			else
				item = item .. c
			end
		end
		i = i + 1
	end
	item = item:gsub("\\n", "\n")
	table.insert(result, item)
	return result
end

function MetadataImporter.activeSource()
	local catalog = LrApplication.activeCatalog()
	local sources = catalog:getActiveSources()
	local count = 0
	for _ in pairs(sources) do count = count + 1 end
	if count == 1 then
		local activeSource = sources[1]
		if type(activeSource) == "table" and (activeSource:type() == "LrCollection" or activeSource:type() == "LrCollectionSet" or activeSource:type() == "LrFolder") then
			return activeSource
		elseif type(activeSource) == "string" and activeSource == catalog.kAllPhotos then
			return activeSource
		end
	end
	return nil
end

function MetadataImporter.preflightCheck()
	if prefs.updateScope == "activeCollection" then
		local activeSource = MetadataImporter.activeSource()
		if activeSource == nil then
			LrDialogs.message("Collection Not Selected", "Please go to the Lightroom Collection or Folder you want to import metadata into.")
			return false
		end
	end
	return true
end

function MetadataImporter.saveLines( lines )
	local selectedFile = LrDialogs.runSavePanel( { title = "Save CSV", prompt = "Save", requiredFileType = "csv", canCreateDirectories = true } )
	if selectedFile ~= nil then
		local f = assert(io.open(selectedFile, "w"))
		f:write(lines)
		f:close()
	end
end

function MetadataImporter.importMetadataFromCSVLines( parameters )

	LrFunctionContext.callWithContext( "MetadataImporterContext", function( functionContext )

		if progressScope == nil then
			progressScope = LrDialogs.showModalProgressDialog( { title = "Metadata Import", cannotCancel = true, functionContext = functionContext } )
			progressScope:setIndeterminate( true )
		end

		local catalog = LrApplication.activeCatalog()

		catalog:withWriteAccessDo("ShutterSnitch Metadata Import", function()
			
			local csvLines = parameters["lines"]
			local networkSyncRequestTime = parameters["networkSyncRequestTime"]
	
			local numberOfMatches = 0
			local numberOfLines = 0
			local indexFor = nil
			local values = {}
			local stars = 0
			local searchParms = {}
			local foundPhotos = {}
			local keywords = {}
			local keyword = nil
			local keywordStr = ""
			local debugFilePaths = nil
			local headerLineItems
			local itemsPerLine
			local folderPathComponents = nil
			local smartCollectionSearchDescription = nil

			local lightroomVersion = LrApplication.versionTable()
		
			if prefs.debugOfferSavingMatchedResults then
				debugFilePaths = ""
			end
		
			local activeSource = nil
			if prefs.updateScope == "activeCollection" and networkSyncRequestTime == nil then
				activeSource = MetadataImporter.activeSource()
				if activeSource == catalog.kAllPhotos then
					activeSource = nil -- just use Entire Collection scope
				end
			end
			if activeSource ~= nil then
				if activeSource:type() == "LrFolder" then
					if WIN_ENV then
						folderPathComponents = activeSource:getPath():gsub("\\", " ")
					else
						folderPathComponents = activeSource:getPath():gsub("/", " ")
					end
				elseif (activeSource:type() == "LrCollection" or activeSource:type() == "LrCollectionSet") and activeSource:isSmartCollection() then
					smartCollectionSearchDescription = activeSource:getSearchDescription()
				end
			end
		
			progressScope:setCaption( "Matching up photos in catalog (grab coffee..)" )
		
			for line in string.gmatch(csvLines,'[^\r\n]+') do
				numberOfLines = numberOfLines + 1
				if prefs.confirmBeforeUpdating or numberOfMatches == 0 then
					values = csvItems(line)
					if indexFor == nil then
	
						headerLineItems = values
						indexFor = {}
						itemsPerLine = #values
						for i = 1, itemsPerLine do
							indexFor[values[i]] = i
						end
		
					elseif values[indexFor["Filename"]] ~= nil then

						if itemsPerLine ~= #values then
							progressScope:done()
							if LrDialogs.confirm("Bad Data", string.format("Line %d should have %d values, but has %d. Cancelling update.", numberOfLines, itemsPerLine, #values), "OK", "Save Debugging Info", nil) == "cancel" then
								local saveStr = ""
								for b = 1, #headerLineItems do
									saveStr = saveStr .. headerLineItems[b] .. "\n"
								end
								saveStr = saveStr .. "\n"
								for b = 1, #values do
									saveStr = saveStr .. values[b] .. "\n"
								end
								saveStr = saveStr .. "\n" .. csvLines
								MetadataImporter.saveLines( saveStr )
							end
							return
						end
						
						searchParms = {
							{
								criteria = "filename",
								operation = "beginsWith",
								value = string.format("%s.", values[indexFor["Filename"]]:match("(.+)%..+$")),
								value2 = "",
							},
							combine = "intersect",
						}
						if activeSource ~= nil then
							if activeSource:type() == "LrFolder" then
								table.insert(searchParms, {
									criteria = "folder",
									operation = "all",
									value = folderPathComponents,
									value2 = "",
								})
							elseif smartCollectionSearchDescription ~= nil then
								table.insert(searchParms, smartCollectionSearchDescription)
							else
								table.insert(searchParms, {
									criteria = "collection",
									operation = "equals",
									value = activeSource:getName(),
									value2 = "",
								})
							end
						end
						if values[indexFor["Capture Time"]] ~= nil then
							table.insert(searchParms, {
								criteria = "captureTime",
								operation = "==",
								value = explode(values[indexFor["Capture Time"]], ' ')[1], -- strip the time
								value2 = "",
							})
						end
						foundPhotos = catalog:findPhotos {
							sort = "captureTime",
								 ascending = true,
								 searchDesc = searchParms
						}
						if #foundPhotos > 0 then
							if debugFilePaths ~= nil then
								for n = 1, #foundPhotos do
									local collstr = ""
									local coll = foundPhotos[n]:getContainedCollections()
									for idx = 1, #coll do
										collstr = collstr .. string.format(" '%s'", coll[idx]:getName())
									end
									debugFilePaths = debugFilePaths .. string.format("%s + %s => %s in%s\n", values[indexFor["Filename"]], values[indexFor["Capture Time"]], foundPhotos[n]:getRawMetadata("path"), collstr)
								end
							end
							numberOfMatches = numberOfMatches + #foundPhotos
						end
					end
				end
			end
	
			if numberOfMatches > 0 then

				local pride
				if numberOfMatches == 1 then
					pride = "1 local match found"
				else
					pride = string.format("%d local matches found", numberOfMatches)
				end
				local confirmationChoice = nil
				if prefs.confirmBeforeUpdating then
					if debugFilePaths ~= nil then
						confirmationChoice = LrDialogs.confirm("Import", pride, "Update", "Save File Paths for Matches", "Save CSV")
					else
						confirmationChoice = LrDialogs.confirm("Import", pride, "Update", "Cancel", "Save CSV")
					end
				else
					confirmationChoice = "ok"
				end
				if confirmationChoice == "other" then
					
					MetadataImporter.saveLines( csvLines )
					
				elseif debugFilePaths ~= nil and confirmationChoice == "cancel" then
				
					local selectedFile = LrDialogs.runSavePanel( { title = "Save File Paths", prompt = "Save", requiredFileType = "txt", canCreateDirectories = true } )
					if selectedFile ~= nil then
						local f = assert(io.open(selectedFile, "w"))
						f:write(debugFilePaths)
						f:close()
					end
				
				elseif confirmationChoice == "ok" then
	
					progressScope:setCaption( "Updating metadata" )
					progressScope:setIndeterminate( false )
					indexFor = nil
					local lines = string.gmatch(csvLines,'[^\r\n]+')
					local createdKeywords = {}
					local progress = 0
					local colorName
					local red
					local green
					local blue
					local parms
					for line in lines do

						progressScope:setPortionComplete( progress, numberOfLines )
						progress = progress + 1

						values = csvItems(line)
						if indexFor == nil then

							indexFor = {}
							for i = 1, #values do
								indexFor[values[i]] = i
							end
							if values[indexFor["StarRating"]] ~= nil and values[indexFor["Rating"]] == nil then
								indexFor["Rating"] = indexFor["StarRating"]
							end

						elseif values[indexFor["Filename"]] ~= nil then

							if itemsPerLine ~= #values then
								progressScope:done()
								if LrDialogs.confirm("Bad Data", string.format("Line %d should have %d values, but has %d. Cancelling update.", numberOfLines, itemsPerLine, #values), "OK", "Save CSV", nil) == "cancel" then
									MetadataImporter.saveLines( csvLines )
								end
								return
							end

							progressScope:setCaption( string.format( "Finding matches for %s", values[indexFor["Filename"]] ) )

							searchParms = {
								{
									criteria = "filename",
									operation = "beginsWith",
									value = string.format("%s.", values[indexFor["Filename"]]:match("(.+)%..+$")),
									value2 = "",
								},
								combine = "intersect",
							}
							if activeSource ~= nil then
								if activeSource:type() == "LrFolder" then
									local folderPathComponents
									if WIN_ENV then
										folderPathComponents = activeSource:getPath():gsub("\\", " ")
									else
										folderPathComponents = activeSource:getPath():gsub("/", " ")
									end
									table.insert(searchParms, {
										criteria = "folder",
										operation = "all",
										value = folderPathComponents,
										value2 = "",
									})
								elseif smartCollectionSearchDescription ~= nil then
									table.insert(searchParms, smartCollectionSearchDescription)
								else
									table.insert(searchParms, {
										criteria = "collection",
										operation = "equals",
										value = activeSource:getName(),
										value2 = "",
									})
								end
							end
							if values[indexFor["Capture Time"]] ~= nil then
								table.insert(searchParms, {
									criteria = "captureTime",
									operation = "==",
									value = explode(values[indexFor["Capture Time"]], ' ')[1], -- strip the time
									value2 = "",
								})
							end
							foundPhotos = catalog:findPhotos {
								sort = "captureTime",
								ascending = true,
								searchDesc = searchParms
							} 

							for i = 1, #foundPhotos do
								progressScope:setCaption( string.format( "Updating metadata of %s", foundPhotos[i]:getFormattedMetadata("fileName" ) ) )
							
								if lightroomVersion.major >= 2 then
							
									if values[indexFor["Description"]] ~= nil then
										foundPhotos[i]:setRawMetadata("caption", values[indexFor["Description"]])
									end
									if values[indexFor["Byline"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creator", values[indexFor["Byline"]])
									end
									if values[indexFor["Copyright"]] ~= nil then
										foundPhotos[i]:setRawMetadata("copyright", values[indexFor["Copyright"]])
									end
									if values[indexFor["Copyright URL"]] ~= nil then
										foundPhotos[i]:setRawMetadata("copyrightInfoUrl", values[indexFor["Copyright URL"]])
									end
									if values[indexFor["Credit"]] ~= nil then
										foundPhotos[i]:setRawMetadata("provider", values[indexFor["Credit"]])
									end
									if values[indexFor["Creator's Job Title"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorJobTitle", values[indexFor["Creator's Job Title"]])
									end
									if values[indexFor["Creator Contact Info Address"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorAddress", values[indexFor["Creator Contact Info Address"]])
									end
									if values[indexFor["Creator Contact Info City"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorCity", values[indexFor["Creator Contact Info City"]])
									end
									if values[indexFor["Creator Contact Info State/Province"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorStateProvince", values[indexFor["Creator Contact Info State/Province"]])
									end
									if values[indexFor["Creator Contact Info Postal Code"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorPostalCode", values[indexFor["Creator Contact Info Postal Code"]])
									end
									if values[indexFor["Creator Contact Info Country"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorCountry", values[indexFor["Creator Contact Info Country"]])
									end
									if values[indexFor["Creator Contact Info Phone"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorPhone", values[indexFor["Creator Contact Info Phone"]])
									end
									if values[indexFor["Creator Contact Info Email"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorEmail", values[indexFor["Creator Contact Info Email"]])
									end
									if values[indexFor["Creator Contact Info URL"]] ~= nil then
										foundPhotos[i]:setRawMetadata("creatorUrl", values[indexFor["Creator Contact Info URL"]])
									end
									if values[indexFor["Label"]] ~= nil then
										foundPhotos[i]:setRawMetadata("label", values[indexFor["Label"]])
									end
									if values[indexFor["Label Color"]] ~= nil then
										colorName = values[indexFor["Label Color"]]
										if #colorName == 0 then
											colorName = "none"
										elseif #colorName == 7 and colorName:sub(1,1) == "#" then
											red = tonumber(colorName:sub(2,2 + 1), 16)
											green = tonumber(colorName:sub(4,4 + 1), 16)
											blue = tonumber(colorName:sub(6,6 + 1), 16)
											--LrDialogs.message("Color Components", string.format("%d,%d,%d", red, green, blue))
											if red > 128 and green < 128 and blue < 128 then
												colorName = "red"
											elseif red < 128 and green > 128 and blue < 128 then
												colorName = "green"
											elseif red < 128 and green < 192 and blue > 128 then
												colorName = "blue"
											elseif red > 128 and green < 128 and blue > 128 then
												colorName = "purple"
											elseif red > 128 and green > 128 and blue < 128 then
												colorName = "yellow"
											end
											--LrDialogs.message("Selected Color", colorName)
											foundPhotos[i]:setRawMetadata("colorNameForLabel", colorName)
										end
									end
									if values[indexFor["Headline"]] ~= nil then
										foundPhotos[i]:setRawMetadata("headline", values[indexFor["Headline"]])
									end
									if values[indexFor["Scene"]] ~= nil then
										foundPhotos[i]:setRawMetadata("scene", values[indexFor["Scene"]])
									end
									if values[indexFor["Object Name"]] ~= nil then
										foundPhotos[i]:setRawMetadata("title", values[indexFor["Object Name"]])
									end
									if values[indexFor["Usage Terms"]] ~= nil then
										foundPhotos[i]:setRawMetadata("rightsUsageTerms", values[indexFor["Usage Terms"]])
									end
									if values[indexFor["Sub Location"]] ~= nil then
										foundPhotos[i]:setRawMetadata("location", values[indexFor["Sub Location"]])
									end
									if values[indexFor["City"]] ~= nil then
										foundPhotos[i]:setRawMetadata("city", values[indexFor["City"]])
									end
									if values[indexFor["Province/State"]] ~= nil then
										foundPhotos[i]:setRawMetadata("stateProvince", values[indexFor["Province/State"]])
									end
									if values[indexFor["Country"]] ~= nil then
										foundPhotos[i]:setRawMetadata("country", values[indexFor["Country"]])
									end
									if values[indexFor["Country Code"]] ~= nil then
										foundPhotos[i]:setRawMetadata("isoCountryCode", values[indexFor["Country Code"]])
									end
									if values[indexFor["Source"]] ~= nil then
										foundPhotos[i]:setRawMetadata("source", values[indexFor["Source"]])
									end
									if values[indexFor["Transmission Reference"]] ~= nil then
										foundPhotos[i]:setRawMetadata("jobIdentifier", values[indexFor["Transmission Reference"]])
									end
									if values[indexFor["Special Instructions"]] ~= nil then
										foundPhotos[i]:setRawMetadata("instructions", values[indexFor["Special Instructions"]])
									end
									if values[indexFor["Subject Reference"]] ~= nil then
										foundPhotos[i]:setRawMetadata("iptcSubjectCode", values[indexFor["Subject Reference"]])
									end
									if values[indexFor["Category"]] ~= nil then
										foundPhotos[i]:setRawMetadata("iptcCategory", values[indexFor["Category"]])
									end
									if values[indexFor["Supplemental Categories"]] ~= nil then
										foundPhotos[i]:setRawMetadata("iptcOtherCategories", values[indexFor["Supplemental Categories"]])
									end
									if values[indexFor["Intellectual Genre"]] ~= nil then
										foundPhotos[i]:setRawMetadata("intellectualGenre", values[indexFor["Intellectual Genre"]])
									end
									if values[indexFor["Writer/Editor"]] ~= nil then
										foundPhotos[i]:setRawMetadata("descriptionWriter", values[indexFor["Writer/Editor"]])
									end

									if values[indexFor["Rating"]] ~= nil then
										stars = #values[indexFor["Rating"]]:gsub("★", "*")
										if stars == 0 then
											foundPhotos[i]:setRawMetadata("rating", nil)
										else
											foundPhotos[i]:setRawMetadata("rating", stars)
										end
									end
							
									if values[indexFor["Copyright Status"]] ~= nil then
										if values[indexFor["Copyright Status"]] == "True" then
											foundPhotos[i]:setRawMetadata("copyrightState", "copyrighted")
										elseif values[indexFor["Copyright Status"]] == "False" then
											foundPhotos[i]:setRawMetadata("copyrightState", "public domain")
										else
											foundPhotos[i]:setRawMetadata("copyrightState", "unknown")
										end
									end
							
								end

								if lightroomVersion.major >= 3 then
							
									if values[indexFor["People Shown"]] ~= nil then
										foundPhotos[i]:setRawMetadata("personShown", values[indexFor["People Shown"]])
									end
									if values[indexFor["Event"]] ~= nil then
										foundPhotos[i]:setRawMetadata("event", values[indexFor["Event"]])
									end
									if values[indexFor["Featured Organization Code"]] ~= nil then
										foundPhotos[i]:setRawMetadata("codeOfOrgShown", values[indexFor["Featured Organization Code"]])
									end
									if values[indexFor["Featured Organization Name"]] ~= nil then
										foundPhotos[i]:setRawMetadata("nameOfOrgShown", values[indexFor["Featured Organization Name"]])
									end
									if values[indexFor["Model Age"]] ~= nil then
										foundPhotos[i]:setRawMetadata("modelAge", values[indexFor["Model Age"]])
									end
									if values[indexFor["Model Age Disclosure"]] ~= nil then
										foundPhotos[i]:setRawMetadata("minorModelAge", values[indexFor["Model Age Disclosure"]])
									end
									if values[indexFor["Model Release IDs"]] ~= nil then
										foundPhotos[i]:setRawMetadata("modelReleaseID", values[indexFor["Model Release IDs"]])
									end
									if values[indexFor["Model Release Status"]] ~= nil then
										foundPhotos[i]:setRawMetadata("modelReleaseStatus", values[indexFor["Model Release Status"]])
									end
									if values[indexFor["Additional Model Info"]] ~= nil then
										foundPhotos[i]:setRawMetadata("additionalModelInfo", values[indexFor["Additional Model Info"]])
									end
									if values[indexFor["Image Supplier Image ID"]] ~= nil then
										foundPhotos[i]:setRawMetadata("imageSupplierImageId", values[indexFor["Image Supplier Image ID"]])
									end
									if values[indexFor["Property Release IDs"]] ~= nil then
										foundPhotos[i]:setRawMetadata("propertyReleaseID", values[indexFor["Property Release IDs"]])
									end
									if values[indexFor["Property Release Status"]] ~= nil then
										foundPhotos[i]:setRawMetadata("propertyReleaseStatus", values[indexFor["Property Release Status"]])
									end
									if values[indexFor["Digital Source Type"]] ~= nil then
										foundPhotos[i]:setRawMetadata("sourceType", values[indexFor["Digital Source Type"]])
									end

									if values[indexFor["Location Created"]] ~= nil then
										parms = {}
										local sublocationItems = csvItems(values[indexFor["Location Created"]])
										local countryCodeItems = csvItems(values[indexFor["Location Created: ISO Country Code"]])
										local provinceStateItems = csvItems(values[indexFor["Location Created: Province / State"]])
										local cityItems = csvItems(values[indexFor["Location Created: City"]])
										local countryNameItems = csvItems(values[indexFor["Location Created: Country"]])
										local regionItems = csvItems(values[indexFor["Location Created: World Region"]])
										for c = 1, #sublocationItems do
											table.insert(parms, {
												CountryCode = countryCodeItems[c],
												ProvinceState = provinceStateItems[c],
												City = cityItems[c],
												Sublocation = sublocationItems[c],
												CountryName = countryNameItems[c],
												WorldRegion = regionItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("locationCreated", parms)
										end
									end
							
									if values[indexFor["Location Shown"]] ~= nil then
										parms = {}
										local sublocationItems = csvItems(values[indexFor["Location Shown"]])
										local countryCodeItems = csvItems(values[indexFor["Location Shown: ISO Country Code"]])
										local provinceStateItems = csvItems(values[indexFor["Location Shown: Province / State"]])
										local cityItems = csvItems(values[indexFor["Location Shown: City"]])
										local countryNameItems = csvItems(values[indexFor["Location Shown: Country"]])
										local regionItems = csvItems(values[indexFor["Location Shown: World Region"]])
										for c = 1, #sublocationItems do
											table.insert(parms, {
												CountryCode = countryCodeItems[c],
												ProvinceState = provinceStateItems[c],
												City = cityItems[c],
												Sublocation = sublocationItems[c],
												CountryName = countryNameItems[c],
												WorldRegion = regionItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("locationShown", parms)
										end
									end
									
									if values[indexFor["Artwork Or Object"]] ~= nil then
										parms = {}
										local titleItems = csvItems(values[indexFor["Artwork Or Object"]])
										local creatorItems = csvItems(values[indexFor["Artwork Or Object: Creator"]])
										local dateCreatedItems = csvItems(values[indexFor["Artwork Or Object: Date Created"]])
										local sourceItems = csvItems(values[indexFor["Artwork Or Object: Source"]])
										local sourceInvItems = csvItems(values[indexFor["Artwork Or Object: Source Inventory Number"]])
										local copyrightItems = csvItems(values[indexFor["Artwork Or Object: Copyright Notice"]])
										for c = 1, #titleItems do
											table.insert(parms, {
												AOTitle = titleItems[c],
												AOCreator = creatorItems[c],
												AODateCreated = dateCreatedItems[c],
												AOSource = sourceItems[c],
												AOSourceInvNo = sourceInvItems[c],
												AOCopyrightNotice = copyrightItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("artworksShown", parms)
										end
									end
							
									if values[indexFor["Image Supplier Name"]] ~= nil then
										parms = {}
										local nameItems = csvItems(values[indexFor["Image Supplier Name"]])
										local idItems = csvItems(values[indexFor["Image Supplier Identifier"]])
										for c = 1, #nameItems do
											table.insert(parms, {
												ImageSupplierName = nameItems[c],
												ImageSupplierID = idItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("imageSupplier", parms)
										end
									end
							
									if values[indexFor["Registry Organization ID"]] ~= nil then
										parms = {}
										local orgItems = csvItems(values[indexFor["Registry Organization ID"]])
										local idItems = csvItems(values[indexFor["Registry Image ID"]])
										for c = 1, #orgItems do
											table.insert(parms, {
												RegOrgId = orgItems[c],
												RegItemId = idItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("registryId", parms)
										end
									end
							
									if values[indexFor["Image Creator Name"]] ~= nil then
										parms = {}
										local nameItems = csvItems(values[indexFor["Image Creator Name"]])
										local idItems = csvItems(values[indexFor["Image Creator ID"]])
										for c = 1, #nameItems do
											table.insert(parms, {
												ImageCreatorName = nameItems[c],
												ImageCreatorID = idItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("imageCreator", parms)
										end
									end

									if values[indexFor["Copyright Owner Name"]] ~= nil then
										parms = {}
										local nameItems = csvItems(values[indexFor["Copyright Owner Name"]])
										local idItems = csvItems(values[indexFor["Copyright Owner ID"]])
										for c = 1, #nameItems do
											table.insert(parms, {
												CopyrightOwnerName = nameItems[c],
												CopyrightOwnerID = idItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("copyrightOwner", parms)
										end
									end

									if values[indexFor["Licensor Name"]] ~= nil then
										parms = {}
										local nameItems = csvItems(values[indexFor["Licensor Name"]])
										local idItems = csvItems(values[indexFor["Licensor ID"]])
										local telephone1Items = csvItems(values[indexFor["Licensor Telephone 1"]])
										local telephone2Items = csvItems(values[indexFor["Licensor Telephone 2"]])
										local emailItems = csvItems(values[indexFor["Licensor Email"]])
										local urlItems = csvItems(values[indexFor["Licensor URL"]])
										for c = 1, #nameItems do
											table.insert(parms, {
												LicensorName = nameItems[c],
												LicensorID = idItems[c],
												LicensorTelephone1 = telephone1Items[c],
												LicensorTelephone2 = telephone2Items[c],
												LicensorEmail = emailItems[c],
												LicensorURL = urlItems[c],
											})
										end
										if #parms > 0 then
											foundPhotos[i]:setRawMetadata("licensor", parms)
										end
									end

									if values[indexFor["Keywords"]] ~= nil then
										for key,value in ipairs(foundPhotos[i]:getRawMetadata("keywords")) do foundPhotos[i]:removeKeyword(value) end
										keywords = explode(values[indexFor["Keywords"]], ',')
										for ki = 1, #keywords do
											keywordStr = LrStringUtils.trimWhitespace(keywords[ki])
											if #keywordStr > 0 then
												keyword = createdKeywords[keywordStr]
												if keyword == nil then
													keyword = catalog:createKeyword(keywordStr, {}, true, nil , true)
													createdKeywords[keywordStr] = keyword
												end
												if keyword ~= nil then
													foundPhotos[i]:addKeyword(keyword)
												end
											end
										end
									end

								end

								if lightroomVersion.major >= 4 then
							
									if values[indexFor["Geolocation"]] ~= nil then
										local coordinates = values[indexFor["Geolocation"]]
										-- Ex. "55°41.388N 12°32.075E"
										local vals = explode(coordinates, ' ')
										if #vals == 2 then
											local latitude = explode(vals[1], '°')
											latitude = tonumber(latitude[1]) + tonumber(latitude[2]:sub(1,#latitude[2] - 1)) / 60.0
											if vals[1]:sub(#vals[1]) == 'S' then
												latitude = latitude * -1.0
											end
											local longitude = explode(vals[2], '°')
											longitude = tonumber(longitude[1]) + tonumber(longitude[2]:sub(1,#longitude[2] - 1)) / 60.0
											if vals[2]:sub(#vals[2]) == 'W' then
												longitude = longitude * -1.0
											end
											if latitude ~= nil and longitude ~= nil then
												foundPhotos[i]:setRawMetadata("gps", { latitude = latitude, longitude = longitude })
											end
										end
									end
									if values[indexFor["GPS Altitude"]] ~= nil then
										foundPhotos[i]:setRawMetadata("gpsAltitude", tonumber(values[indexFor["GPS Altitude"]]))
									end

								end
							
								if lightroomVersion.major >= 6 then
							
									if values[indexFor["GPS Direction"]] ~= nil then
										foundPhotos[i]:setRawMetadata("gpsImgDirection", tonumber(values[indexFor["GPS Direction"]]))
									end
								
								end
							
							end
						end
					end
					if networkSyncRequestTime ~= nil then
						prefs.syncSince = networkSyncRequestTime
					end
				end

			elseif networkSyncRequestTime == nil then
				local message = "No file matches found in "
				if activeSource == nil then
					message = message .. "catalog"
				else
					message = message .. "'" .. activeSource:getName() .. "'"
				end
				if LrDialogs.confirm("Import", message, "OK", "Save CSV", nil) == "cancel" then
					MetadataImporter.saveLines( csvLines )
				end
			else
				prefs.syncSince = networkSyncRequestTime
			end

			progressScope:done()

		end )
		
	end)
end

function MetadataImporter.networkImport( parameters )

	local hostAddress = prefs.hostAddress
	if hostAddress == nil or #hostAddress == 0 then
		LrDialogs.message("No Host Address", "Please go to File > Plug-in Manager > ShutterSnitch, and add the IP address you see at the top of the screen in ShutterSnitch when entering a collection.")
		return
	end
	
	local query = parameters["query"]
	if query == nil then
		LrDialogs.showError("Missing 'query' parameter")
		return
	end

	LrFunctionContext.callWithContext( "MetadataImporterContext", function( functionContext )

		progressScope = LrDialogs.showModalProgressDialog( { title = "Metadata Import", cannotCancel = true, functionContext = functionContext } )
		progressScope:setIndeterminate( true )
		progressScope:setCaption( "Authenticating" )
		local networkSyncRequestTime = parameters["networkSyncRequestTime"]
		local result, hdrs = LrHttp.get( string.format("http://%s:46786/lrplugin?", hostAddress), nil, 10 )
		result = LrStringUtils.decodeBase64(result)
		if result == nil then
			LrDialogs.message("No Reply", string.format("Make sure ShutterSnitch is open, on the same network as this computer, and the address in the plug-in settings (%s) matches the one you see at the top of the screen when tapping the title bar in ShutterSnitch.", hostAddress))
		elseif #result > 0 then
			local passwordHash = LrPasswords.retrieve( hostAddress , 'p3ppA', nil )
			if passwordHash == nil then
				passwordHash = ""
			end
			local headers
			local requestPath = string.format("/lrplugin?%s", query)
			while (true) do
		
				progressScope:setCaption( "Requesting latest changes" )
				headers = {
					{ field = 'Auth', value = LrMD5.digest( result:sub(8)..requestPath..passwordHash ) }
				}
				result, hdrs = LrHttp.get( string.format("http://%s:46786%s", hostAddress, requestPath), headers, 60 )
				result = LrStringUtils.decodeBase64(result)
				if result ~= nil and #result > 0 then
					if result:find("Nonce: ", 1) == 1 then

						local f = LrView.osFactory()
						local passwordField = f:password_field {
							immediate = true,
							value = ""
						}
						local c = f:column {
							spacing = f:dialog_spacing(),
							f:row {
								fill_horizontal  = 1,
								f:static_text {
									alignment = "left",
									title = "ShutterSnitch Password:"
								},
							},
							f:row {
								passwordField,
							},
						} 
						if LrDialogs.presentModalDialog { title = "Please Authenticate", contents = c } == "ok" then
							passwordHash = LrMD5.digest( passwordField.value.."rnKug2" )
							passwordField.value = ""
							LrPasswords.store( hostAddress, passwordHash, 'p3ppA', nil )
						else
							break
						end

					elseif result:find("Error: ", 1) == 1 then
						LrDialogs.message("Error", result:sub(8))
						break
					else
						MetadataImporter.importMetadataFromCSVLines {
							lines = result,
							networkSyncRequestTime = networkSyncRequestTime,
						}
						break
					end
				else
					break
				end
			end
		end
		
	end )

end